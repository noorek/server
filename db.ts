import {Pool} from "pg";

const pool = new Pool({
  user: "test",
  password: "test",
  host: "postgres",
  port: 5432,
  database: "store"
});

export default pool;