import {Product} from "./types/Product";

export const sampleProducts: Product[] = [
  {
    id: "milk-1",
    name: "Milk",
    image_url: "../images/milk-1.jpg",
    price: 1.5,
    amount: 1,
  },
  {
    id: "snickers-1",
    name: "Snickers",
    image_url: "../images/snickers-1.jpg",
    price: 0.5,
    amount: 1,
  },
  {
    id: "kiri-1",
    name: "Kiri Cheese",
    image_url: "../images/kiri-1.avif",
    price: 0.75,
    amount: 1,
  },
  {
    id: "mars-1",
    name: "Mars",
    image_url: "../images/mars-1.jpg",
    price: 0.5,
    amount: 1,
  },
  {
    id: "ketchup-1",
    name: "Ketchup",
    image_url: "../images/ketchup-1.avif",
    price: 1,
    amount: 1,
  },
  {
    id: "vimto-1",
    name: "Vimto",
    image_url: "../images/vimto-1.avif",
    price: 2.5,
    amount: 1,
  },
  {
    id: "aquafina-1",
    name: "Aquafina",
    image_url: "../images/aquafina-1.jpg",
    price: 1,
    amount: 1,
  },
  {
    id: "icecream-1",
    name: "Icecream",
    image_url: "../images/icecream-1.jpg",
    price: 2.5,
    amount: 1,
  },
]