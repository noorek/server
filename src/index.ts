import express from "express";
import {sampleProducts} from "./data";
import cors from "cors";
import pool from "../db";
import {Product} from "./types/Product";
import {getTotalPrice} from "./functions/getTotalPrice";
import {v4 as genuuid} from "uuid";
import {Cart, CartProduct} from "./types/Cart";

const app = express();
app.use(cors({
  origin: ["http://localhost:5173", "http://localhost"]
}))
const port = 4001;

const parser = express.text({type: "application/json"});

app.get('/api/products', async (req, res) => {
  const products = await pool.query("SELECT * from products ORDER by id");
  return res.status(200).json(products.rows);
})

app.get('/api/carts', async (req, res) => {
  const carts = await pool.query("SELECT * from carts WHERE status = $1 ORDER by id", ["pending"]);
  const cart_rows: Cart[] = carts.rows;
  const new_carts = await Promise.all(cart_rows.map(async ({id, cart_product_ids, status, total_price}) => {
    const products = await Promise.all(cart_product_ids.map(async cart_product_id => {
      const cart_product = await pool.query("SELECT * from cart_products WHERE id = $1", [cart_product_id]);
      const cart_product_row: CartProduct = cart_product.rows[0];
      const product = await pool.query("SELECT * from products WHERE id = $1", [cart_product_row.product_id]);
      const product_row: Product = product.rows[0];
      return {...product_row, amount: cart_product_row.amount}
    }));

    return {
      cart_id: id,
      status,
      total_price,
      products
    }
  }))
  return res.status(200).json(new_carts)
})

app.get('/api/cart_product/:id', async (req, res) => {
  const cart_product = await pool.query("SELECT * from cart_products WHERE id = $1", [req.params.id]);
  return res.status(200).json(cart_product.rows)
})

app.post('/api/checkout', parser, async (req, res) => {
  const products: Product[] = JSON.parse(req.body);
  if (!products || products.length === 0)
    throw new Error("Missing products on checkout");

  const total_price = getTotalPrice(products);
  console.log(products, total_price);

  const cart_id = genuuid();
  const cart_product_id = (product_id: string) => cart_id + ':' + product_id;
  // Create cart product object
  products.map(product =>
    pool.query("INSERT INTO cart_products (id, product_id, amount) VALUES ($1, $2, $3) RETURNING *", [
      cart_product_id(product.id),
      product.id,
      product.amount,
    ]));
  // Create cart object
  pool.query("INSERT INTO carts (id, cart_product_ids, total_price, status) VALUES ($1, $2, $3, $4) RETURNING *", [
    cart_id,
    products.map(p => cart_product_id(p.id)),
    total_price,
    "pending"
  ])

  return res.status(200);
})

app.put('/api/carts/update_status/:id', parser, async (req, res) => {
  const {status}: {status: Cart["status"]} = JSON.parse(req.body);
  if (!status || (status !== "completed" && status !== "pending" && status !== "rejected"))
    return res.status(400).json("Invalid parameters for status")
  const updated_cart = await pool.query("UPDATE carts SET status = $1 WHERE id = $2 RETURNING *", [status, req.params.id])
  return res.status(200).json(updated_cart.rows);
})

app.listen(port, () => console.log(`Server start at ${port}`))