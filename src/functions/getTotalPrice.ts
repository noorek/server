import {Product} from "../types/Product";

export const getTotalPrice = (products: Product[]) =>
  products.reduce((acc, product) => acc + product.amount * product.price, 0);